import com.fasterxml.jackson.databind.ObjectMapper
import com.github.javafaker.Faker
import fr.uge.java.Vaccine
import fr.uge.java.kafka.SideEffectRecord

import java.util.HashMap
import org.apache.jena.rdf.model.{Model, Resource}

import fr.uge.java.SideEffect
import java.util.Date
import scala.collection.mutable.ListBuffer


object FakeInfo{
  var id :Int = 0

  def createFakeInfo(model: Model, subject:Resource, params:List[Int]): SideEffectRecord ={

    //var tmp = new Statement()
    import java.text.SimpleDateFormat

    var record: SideEffectRecord = null;
    val r = scala.util.Random
    val gender = Array("Female", "Male")
    val faker = new Faker();
    val address = faker.address()

    val idProperty = model.createProperty("http://extension.group1.fr/onto#", "id")
    val firstNameProperty = model.createProperty("http://extension.group1.fr/onto#", "firstName")
    val lastNameProperty = model.createProperty("http://extension.group1.fr/onto#", "lastName")
    val genderProperty = model.createProperty("http://extension.group1.fr/onto#", "gender")
    val zipcodeProperty = model.createProperty("http://extension.group1.fr/onto#", "zipcode")
    val vaccinatedProperty = model.createProperty("http://extension.group1.fr/onto#", "vaccinated")
    val vaccinType = model.createProperty("http://extension.group1.fr/onto#", "vaccinType")
    val hasCovidProperty = model.createProperty("http://extension.group1.fr/onto#", "hasCovid")
    val vaccinationDateProperty = model.createProperty("http://extension.group1.fr/onto#", "vaccinationDate")
    val sideEffectsProperty = model.createProperty("http://extension.group1.fr/onto#", "sideEffects")
    val symptomsProperty = model.createProperty("http://extension.group1.fr/onto#", "symptoms")
    val birthdateProperty = model.createProperty("http://extension.group1.fr/onto#", "birthdate")

    val idObj = model.createLiteral(id.toString)
    model.add(model.createStatement(subject, idProperty, idObj))
    val firstNameObj = model.createLiteral(faker.name().firstName())
    model.add(model.createStatement(subject, firstNameProperty, firstNameObj))
    val lastNameObj = model.createLiteral(faker.name().lastName())
    model.add(model.createStatement(subject, lastNameProperty, lastNameObj))
    var genderIndex = 0
    if (r.nextInt(100) > params(0)){
      genderIndex = 1
    }
    val genderObj = model.createLiteral(gender(genderIndex))
    model.add(model.createStatement(subject, genderProperty, genderObj))
    val zipcodeObj = model.createLiteral(address.zipCode())
    model.add(model.createStatement(subject, zipcodeProperty, zipcodeObj))
    val isVaccinated = r.nextBoolean()
    model.add(model.createStatement(subject, vaccinatedProperty, isVaccinated.toString))

    var birthday :Date = null
    if (subject.getLocalName.contains("Student")){
      birthday = faker.date().birthday(20, 30)
    }
    else {
      birthday = faker.date().birthday(30, 70)
    }
    val birthdateObj = model.createLiteral(birthday.toString)
    model.add(model.createStatement(subject, birthdateProperty, birthdateObj))

    if (isVaccinated){
      val vaccinObj = createFakeVaccins(params)
      model.add(model.createStatement(subject, vaccinType, vaccinObj))
      val vaccinationDateObj = model.createLiteral(faker.date().between(new SimpleDateFormat("dd/MM/yyyy").parse("01/06/2019"), new Date()).toString)
      model.add(model.createStatement(subject, vaccinationDateProperty, vaccinationDateObj))
      /*
      val hasSideEffects = r.nextBoolean()
      if (hasSideEffects){
      50% chance to have side effects then
       */
        val sideEffect = SideEffect.randomSideEffect();
        model.add(model.createStatement(subject, sideEffectsProperty, model.createLiteral(sideEffect.getName())));

      /*
        // create json
        var  tmp = new util.HashMap[String, String]();


        tmp.put("id", idObj.toString())
        tmp.put("firstName", firstNameObj.toString)
        tmp.put("lastName", lastNameObj.toString())
        tmp.put("vaccinName", vaccinObj.toString())
        tmp.put("sideEffectName", sideEffect.getName())
        tmp.put("siderCode", sideEffect.getSiderCode())
        */
        //println(map)
        record = SideEffectRecord.create(idObj.toString(), firstNameObj.toString(), lastNameObj.toString(), birthday, vaccinObj.toString(), sideEffect.getName(), sideEffect.getSiderCode())
    }

    //println(jsonInfo);
    /*
    val hasCovid = r.nextBoolean()
    val hasCovidObj = model.createLiteral(hasCovid.toString)
    model.add(model.createStatement(subject, hasCovidProperty, hasCovidObj))
    if (hasCovid){
      model.add(model.createStatement(subject, symptomsProperty, model.createLiteral(createFakeDisease())))
    }
    */


    id = id + 1
    val statement = model.createStatement(subject, idProperty, idObj)
    model.add(statement)
    record
  }

  def createFakeVaccins(params:List[Int]) : String  ={
    val r = scala.util.Random


    //val vaccins = Array("Moderna", "Pfizer", "BBIBP-CorV", "Sputnik_V", "AstraZeneca")
    //val index = r.nextInt(vaccins.length)
    //vaccins(index)
    val vaccines = Vaccine.values()
    val index = r.nextInt(100)


    var i = 2;
    var sum = 0

    for ( i <- 2 to 6){
      sum += params(i)
      if (sum > index){
        return vaccines(i-2).toString
      }
    }



    throw new IllegalStateException("Should not happen")
  }
}
