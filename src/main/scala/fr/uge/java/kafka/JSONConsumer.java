package fr.uge.java.kafka;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;

import java.time.Duration;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class JSONConsumer {
    private static final String TOPIC =  "jsonSideEffect";

    public static void RunConsumer(){
        final Map<String, Object> config = new HashMap<>();
        config.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        config.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        config.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        config.put(ConsumerConfig.GROUP_ID_CONFIG, Settings.GROUP);

        KafkaConsumer<String, String> consumer = new KafkaConsumer<String, String>(config);
        consumer.subscribe(Collections.singletonList(TOPIC));

        new Thread(() -> {
            int noRecordsCount = 0;

        while (true) {
            //ConsumerRecords<String, String> records = consumer.poll(1000);
            ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(1000));
            if (records.count() == 0) {
                noRecordsCount++;
                if (noRecordsCount > Settings.GIVE_UP) break;
                else continue;
            }
            noRecordsCount = 0;

            records.forEach(record -> {
                System.out.printf("Consumer Record:(%s, %s, %d, %d)\n",
                        record.key(), record.value(),
                        record.partition(), record.offset());
            });

            //consumer.commitAsync();
            consumer.commitSync();
        }
        consumer.close();
        //System.out.println("DONE");
        }).start();
    }
}
