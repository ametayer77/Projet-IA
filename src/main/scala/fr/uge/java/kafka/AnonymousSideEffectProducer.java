package fr.uge.java.kafka;

import com.twitter.bijection.Injection;
import com.twitter.bijection.avro.GenericAvroCodecs;
import fr.uge.java.Vaccine;
import org.apache.avro.Schema;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericRecord;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.internals.DefaultPartitioner;
import org.apache.kafka.common.serialization.ByteArraySerializer;
import org.apache.kafka.common.serialization.StringSerializer;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;



public class AnonymousSideEffectProducer {
    private Schema schema;
    private KafkaProducer<String, byte[]> producer;
    private Injection<GenericRecord, byte[]> recordInjection;
    private Set<Integer> allPartitions = new HashSet<>();
    private boolean b;
    private AnonymousSideEffectProducer(Schema schema, KafkaProducer<String, byte[]> producer, Injection<GenericRecord, byte[]> recordInjection){
        this.schema = schema;
        this.producer = producer;
        this.recordInjection = recordInjection;
    }

    /**
     * Create a {@link KafkaProducer} that will anonymize a {@link SideEffectRecord} base on "schema_anonymous_side_effect.avsc" {@link Schema}
     * @return the {@link KafkaProducer}
     * @throws IOException if an I/O errors occurs
     */
    public static AnonymousSideEffectProducer create() throws IOException {
        final Map<String, Object> config = new HashMap<>();
        config.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        config.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, ByteArraySerializer.class);
        config.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        //config.put(ProducerConfig.PARTITIONER_CLASS_CONFIG, DefaultPartitioner.class);
        config.put(ProducerConfig.ACKS_CONFIG, "all");

        File file = Paths.get("resources", "schemas", "schema_anonymous_side_effect.avsc").toFile();
        Schema.Parser parser = new Schema.Parser();
        Schema schema = parser.parse(file);

        KafkaProducer<String, byte[]> producer = new KafkaProducer<>(config);
        Injection<GenericRecord, byte[]> recordInjection = GenericAvroCodecs.toBinary(schema);
        return new AnonymousSideEffectProducer(schema, producer, recordInjection);
    }

    public void add(String id, GenericRecord item){
        GenericRecord genericRecord = new GenericData.Record(schema);
        genericRecord.put("id", id);
        genericRecord.put("vaccineName", item.get("vaccineName"));
        genericRecord.put("sideEffect", item.get("sideEffect"));
        genericRecord.put("siderCode", item.get("siderCode"));
        byte[] data = recordInjection.apply(genericRecord);
        int partition = Vaccine.valueOf(item.get("vaccineName").toString()).ordinal();
        //System.out.println("send to " + Settings.TOPIC_VACCINE + " partition:" + partition);
        producer.send(new ProducerRecord<>(Settings.TOPIC_VACCINE, partition, id, data));
    }

    /**
     * Flush and close consumer
     */
    public void end(){
        producer.flush();
        producer.close();
    }
}
