package fr.uge.java.kafka;

import com.twitter.bijection.Injection;
import com.twitter.bijection.avro.GenericAvroCodecs;
import org.apache.avro.Schema;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericRecord;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.internals.DefaultPartitioner;
import org.apache.kafka.common.serialization.ByteArraySerializer;
import org.apache.kafka.common.serialization.StringSerializer;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

/**
 * Represents a {@link KafkaProducer} that send a anonymized avro message of a {@link SideEffectRecord} on a specific partition
 */
public class SpecificVaccineSideEffectProducer {

    private Schema schema;
    private KafkaProducer<String, byte[]> producer;
    private Injection<GenericRecord, byte[]> recordInjection;

    private SpecificVaccineSideEffectProducer(Schema schema, KafkaProducer<String, byte[]> producer, Injection<GenericRecord, byte[]> recordInjection){
        this.schema = schema;
        this.producer = producer;
        this.recordInjection = recordInjection;
    }

    /**
     * create a {@link KafkaProducer}
     * @return the producer
     * @throws IOException if an I/O error occurs
     */
    public static SpecificVaccineSideEffectProducer create() throws IOException {
        final Map<String, Object> config = new HashMap<>();
        config.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        config.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, ByteArraySerializer.class);
        config.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        //config.put(ProducerConfig.PARTITIONER_CLASS_CONFIG, DefaultPartitioner.class);

        File file = Paths.get("resources", "schemas", "schema_anonymous_side_effect.avsc").toFile();
        Schema.Parser parser = new Schema.Parser();
        Schema schema = parser.parse(file);

        KafkaProducer<String, byte[]> producer = new KafkaProducer<>(config);
        Injection<GenericRecord, byte[]> recordInjection = GenericAvroCodecs.toBinary(schema);
        return new SpecificVaccineSideEffectProducer(schema, producer, recordInjection);
    }

    /**
     * send the anonymized data to an specific partition corresponding of {@link fr.uge.java.Vaccine} ordinal<br>
     * ex: MODERNA -> partition 0
     * @param topic the topic of consumers {@link Settings} TOPIC_VACCINE
     * @param partition the ordinal of {@link fr.uge.java.Vaccine} name
     * @param id the {@link GenericRecord} id
     * @param item the item, avro message representing an ({@link SideEffectRecord} base on "schema_record.avsc" {@link Schema}
     */
    public void add(String topic, int partition, String id, GenericRecord item){
        GenericRecord genericRecord = new GenericData.Record(schema);
        genericRecord.put("id", id);
        genericRecord.put("vaccineName", item.get("vaccineName"));
        genericRecord.put("sideEffect", item.get("sideEffect"));
        genericRecord.put("siderCode", item.get("siderCode"));
        byte[] data = recordInjection.apply(genericRecord);
        System.out.println(" send data with topic " + topic + " and partition " + partition + " id" + id);
        producer.send(new ProducerRecord<>(topic, partition, id, data));
    }

    /**
     * Flush and close consumer
     */
    public void end(){
        producer.flush();
        producer.close();
    }

}
