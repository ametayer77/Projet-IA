package fr.uge.java.kafka;

import org.apache.avro.Schema;
import org.apache.avro.file.SeekableByteArrayInput;
import org.apache.avro.generic.GenericDatumReader;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.io.BinaryDecoder;
import org.apache.avro.io.DecoderFactory;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.ByteArrayDeserializer;
import org.apache.kafka.common.serialization.StringDeserializer;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.time.Duration;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static fr.uge.java.kafka.Settings.POLL_TIME;
import static fr.uge.java.kafka.Settings.TOPIC_AVRO_SIDE_EFFECT;

/**
 * Represents a {@link KafkaConsumer} that read an avro message which represents a {@link SideEffectRecord} from  {@link Schema} "schema_record.avsc"
 */
public class AvroConsumer {


    /**
     * Run a Avro {@link KafkaConsumer} base on  {@link Schema} "schema_record.avsc"<br>
     * This consumer will receive a avro message corresponding to an @{@link SideEffectRecord}<br>
     * This consumer listen on TOPIC_AVRO_SIDE_EFFECT topic from {@link Settings}<br>
     * This consumer will anonymized the avro message and send it on the topic TOPIC_ANONYMOUS from {@link Settings}
     */
    public static void RunConsumer(){
        final Map<String, Object> config = new HashMap<>();
        config.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        config.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, ByteArrayDeserializer.class);
        config.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        config.put(ConsumerConfig.GROUP_ID_CONFIG, TOPIC_AVRO_SIDE_EFFECT);

        KafkaConsumer<String, byte[]> consumer = new KafkaConsumer<>(config);
        consumer.subscribe(Collections.singletonList(TOPIC_AVRO_SIDE_EFFECT));
        new Thread(() -> {
            try {
                int noRecordsCount = 0;
                File file = Paths.get("resources", "schemas", "schema_record.avsc").toFile();
                Schema.Parser parser = new Schema.Parser();
                var anonymousSideEffectProducer =  AnonymousSideEffectProducer.create();
                Schema schema = parser.parse(file);

                while (!Thread.currentThread().isInterrupted()) {
                    //ConsumerRecords<String, byte[]> records = consumer.poll(1000);
                    ConsumerRecords<String, byte[]> records = consumer.poll(Duration.ofMillis(POLL_TIME));

                    if (records.count() == 0) {
                        noRecordsCount++;
                        if (noRecordsCount > Settings.GIVE_UP) break;
                        //else Thread.sleep(1000); continue;
                        else continue;
                    }
                    noRecordsCount = 0;

                    //Schema finalSchema = schema;
                    for (ConsumerRecord<String, byte[]> record : records) {
                        //Injection<GenericRecord, byte[]> recordInjection = GenericAvroCodecs.toBinary(finalSchema);
                        byte[] data = record.value();
                        GenericDatumReader<GenericRecord> datumReader = new GenericDatumReader<>(schema);
                        SeekableByteArrayInput inputStream = new SeekableByteArrayInput(data);
                        BinaryDecoder decoder = DecoderFactory.get().binaryDecoder(inputStream, null);
                        //ArrayList<GenericRecord> l = new ArrayList<>();
                        //while (!decoder.isEnd()) {
                        GenericRecord item = datumReader.read(null, decoder);
                          //  l.add(item);
                        //System.out.println("item sideEffect " + item.get("sideEffect"));
                        //}
                        /*
                        System.out.printf(AvroConsumer.class.getName() + " Consumer Record:(%s, %s, %d, %d)\n",
                                record.key(), item,
                                record.partition(), record.offset());
                        */
                        anonymousSideEffectProducer.add(record.key(), item);
                        //}
                    }

                    consumer.commitAsync();
                    //consumer.commitSync();
                }
                consumer.close();
                anonymousSideEffectProducer.end();
                System.out.println("closing Avro Consumer");
            } catch (IOException e) {
                System.out.println(AvroConsumer.class.getName() + " error while loading schema " + e.getMessage());
                return;
            }
            /*
            catch (InterruptedException e){
                System.out.println(AvroConsumer.class.getName() + " interrupted ");
                return;
            }
            */
        }).start();
    }
}

/*
void lala(){
    File file = Paths.get("resources", "schemas", "schema_record.avsc").toFile();
    Schema.Parser parser = new Schema.Parser();
    Schema schema = null;
    try {
        schema = parser.parse(file);
    } catch (IOException e) {
        System.out.println("error while loading schema");
        return;
    }

 */
    /*
    GenericDatumReader<GenericRecord> datumReader = new GenericDatumReader<GenericRecord>(schema);
            val inputStream = new SeekableByteArrayInput(bytes)
    val decoder = DecoderFactory.get.binaryDecoder(inputStream, null)

    val result = new mutable.MutableList[org.apache.avro.generic.GenericRecord]
    while (!decoder.isEnd) {
        val item = datumReader.read(null, decoder)

        result += item
    }

    result.toList
  }
}
    */
