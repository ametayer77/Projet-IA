package fr.uge.java.kafka;

import fr.uge.java.Vaccine;
import org.apache.kafka.clients.admin.AdminClient;
import org.apache.kafka.clients.admin.AdminClientConfig;
import org.apache.kafka.clients.admin.NewTopic;

import java.util.Collections;
import java.util.Properties;
import java.util.concurrent.ExecutionException;

public class Settings {
    public static final int POLL_TIME = 1000;
    public static final int GIVE_UP = 500;
    public static final String GROUP = "myConsumer";
    public static final String VACCINE_GROUP = "VaccineGroup";
    public static final String TOPIC_AVRO_BASIC_RDF_DATA = "avroBasicRDFData";
    public static final String TOPIC_AVRO_SIDE_EFFECT = "avroSideEffect";
    public static final String TOPIC_ANONYMOUS = "anonymousSideEffect";
    public static final String TOPIC_VACCINE = "VaccineSideEffect";

    public static void initialize() throws ExecutionException, InterruptedException {
        //new NewTopic(Utils.TOPIC_VACCINE, Vaccine.values().length, 1);
        Properties config = new Properties();
        config.put(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        AdminClient admin = AdminClient.create(config);
        boolean alreadyExists = admin.listTopics().names().get().stream()
                .anyMatch(existingTopicName -> existingTopicName.equals(TOPIC_VACCINE));
        if (alreadyExists) {
            System.out.println("already exists");
            return;
        } else {
            //creating new topic
            System.out.printf("creating topic: %s%n", TOPIC_VACCINE);
            NewTopic newTopic = new NewTopic(TOPIC_VACCINE, Vaccine.values().length, (short) 1);
            admin.createTopics(Collections.singleton(newTopic)).all().get();
        }
        rdfTopic(admin);
        mergedTopic(admin);
    }

    public static void rdfTopic(AdminClient admin) throws ExecutionException, InterruptedException {
        boolean alreadyExists = admin.listTopics().names().get().stream()
                .anyMatch(existingTopicName -> existingTopicName.equals(TOPIC_AVRO_BASIC_RDF_DATA));
        if (alreadyExists) {
            System.out.println("already exists " + TOPIC_AVRO_BASIC_RDF_DATA);
            return;
        } else {
            //creating new topic
            System.out.printf("creating topic: %s%n", TOPIC_AVRO_BASIC_RDF_DATA);
            NewTopic newTopic = new NewTopic(TOPIC_AVRO_BASIC_RDF_DATA, Vaccine.values().length, (short) 1);
            admin.createTopics(Collections.singleton(newTopic)).all().get();
        }
    }
    public static void mergedTopic(AdminClient admin) throws ExecutionException, InterruptedException {
        boolean alreadyExists = admin.listTopics().names().get().stream()
                .anyMatch(existingTopicName -> existingTopicName.equals("Merged"));
        if (alreadyExists) {
            System.out.println("already exists " + "Merged");
            return;
        } else {
            //creating new topic
            System.out.printf("creating topic: %s%n", "Merged");
            NewTopic newTopic = new NewTopic("Merged", Vaccine.values().length, (short) 1);
            admin.createTopics(Collections.singleton(newTopic)).all().get();
        }
    }
    public static void flush(){
        try {
            initialize();
        }
        catch (Exception e){
            System.out.println("error while initializing");
            return;
        }
        //SpecificVaccineSideEffectProducer.create().end();
    }
}
