package fr.uge.java.kafka;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class JSONProducer {
    private static final String TOPIC =  "jsonSideEffect";

    public static void runProducer(List<SideEffectRecord> sideEffectRecords){
        final Map<String, Object> config = new HashMap<>();
        config.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        config.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        config.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");

        KafkaProducer<String, String> producer = new KafkaProducer<String, String>(config);
        ObjectMapper mapper = new ObjectMapper();
        for (SideEffectRecord sideEffectRecord : sideEffectRecords) {
            try {
                String jsonResult = mapper.writeValueAsString(sideEffectRecord);
            producer.send(new ProducerRecord<>(TOPIC, sideEffectRecord.getId(), jsonResult));
                Thread.sleep(Settings.POLL_TIME);
            } catch (InterruptedException e) {
                return;
            } catch (JsonProcessingException js) {
                System.out.println("unable to pase to json " + sideEffectRecord);
                continue;
            }
        }
        producer.flush();
        producer.close();
    }
}
