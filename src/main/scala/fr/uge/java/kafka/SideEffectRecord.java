package fr.uge.java.kafka;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.Date;

@JsonPropertyOrder({"id", "firstName", "lastName", "age", "vaccineName", "sideEffect", "siderCode" })
public class SideEffectRecord {
    private String id;
    private String firstName;
    private String lastName;
    private String age;
    private String vaccineName;
    private String sideEffect;
    private String siderCode;

    private SideEffectRecord(String id, String firstName, String lastName, String age, String vaccineName, String sideEffect, String siderCode) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.vaccineName = vaccineName;
        this.sideEffect = sideEffect;
        this.siderCode = siderCode;
    }

    public static SideEffectRecord create(String id, String firstName, String lastName, Date birthDate, String vaccineName, String sideEffect, String siderCode){
        /**/
        Date instant = new Date();
        LocalDate instantLocalDate = instant.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate recordDate = birthDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        String age = Integer.toString(Period.between(recordDate, instantLocalDate).getYears());
        /**/
        return new SideEffectRecord(id, firstName, lastName, age, vaccineName, sideEffect, siderCode);
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setSideEffect(String sideEffect) {
        this.sideEffect = sideEffect;
    }

    public String getSideEffect() {
        return sideEffect;
    }

    public void setSiderCode(String siderCode) {
        this.siderCode = siderCode;
    }

    public String getSiderCode() {
        return siderCode;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setVaccineName(String vaccineName) {
        this.vaccineName = vaccineName;
    }

    public String getVaccineName() {
        return vaccineName;
    }

    /* to use in basic record only */
    public String getAge() {
        return age;
    }

    /* to use in basic record only */
    public void setAge(String age) {
        this.age = age;
    }

    @Override
    public String toString() {
        return "SideEffectRecord{" +
                "id='" + id + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", age='" + age + '\'' +
                ", vaccineName='" + vaccineName + '\'' +
                ", sideEffect='" + sideEffect + '\'' +
                ", siderCode='" + siderCode + '\'' +
                '}';
    }
}
