package fr.uge.java.kafka;

import fr.uge.java.Vaccine;
import org.apache.avro.Schema;
import org.apache.avro.file.SeekableByteArrayInput;
import org.apache.avro.generic.GenericDatumReader;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.io.BinaryDecoder;
import org.apache.avro.io.DecoderFactory;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.ByteArrayDeserializer;
import org.apache.kafka.common.serialization.StringDeserializer;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.time.Duration;
import java.util.*;

import static fr.uge.java.kafka.Settings.POLL_TIME;

/**
 * Represents a {@link KafkaConsumer} that read an avro message which represents  a anonymized {@link SideEffectRecord} from  {@link Schema} "schema_anonymous_side_effect.avsc"
 * Used in precedent version of code only
 */
public class AnonymousSideEffectConsumer {

    public static void RunConsumer(){
        //HashMap<String, Integer> sideEffectOccurence = new HashMap<>();
        final Map<String, Object> config = new HashMap<>();
        config.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        config.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, ByteArrayDeserializer.class);
        config.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        config.put(ConsumerConfig.GROUP_ID_CONFIG, Settings.GROUP);

        KafkaConsumer<String, byte[]> consumer = new KafkaConsumer<>(config);
        consumer.subscribe(Collections.singleton(Settings.TOPIC_ANONYMOUS));
        Set<Integer> allPartitions = new HashSet<>();
        new Thread(() -> {
            int noRecordsCount = 0;
            long count = 0;
            boolean b = false;
            //HashSet<String> allTopics = new HashSet<>();
            File file = Paths.get("resources", "schemas", "schema_anonymous_side_effect.avsc").toFile();
            Schema.Parser parser = new Schema.Parser();
            Schema schema = null;
            try {
                SpecificVaccineSideEffectProducer producer = SpecificVaccineSideEffectProducer.create();
                schema = parser.parse(file);
                //SpecificAnonymousSideEffectConsumer producer = SpecificAnonymousSideEffectConsumer.create();
                while (!Thread.currentThread().isInterrupted()) {
                    //ConsumerRecords<String, byte[]> records = consumer.poll(1000);
                    //ConsumerRecords<String, byte[]> records = consumer.poll(Duration.ofMillis(1000));
                    ConsumerRecords<String, byte[]> records = consumer.poll(Duration.ofMillis(POLL_TIME));

                    //System.out.println(records.count());

                    if (records.count() == 0) {
                        noRecordsCount++;
                        if (noRecordsCount > Settings.GIVE_UP) break;
                        else continue;
                    }
                    noRecordsCount = 0;

                    Schema finalSchema = schema;
                    for (ConsumerRecord<String, byte[]> record : records) {
                        //Injection<GenericRecord, byte[]> recordInjection = GenericAvroCodecs.toBinary(finalSchema);
                        byte[] data = record.value();
                        GenericDatumReader<GenericRecord> datumReader = new GenericDatumReader<GenericRecord>(schema);
                        SeekableByteArrayInput inputStream = new SeekableByteArrayInput(data);
                        BinaryDecoder decoder = DecoderFactory.get().binaryDecoder(inputStream, null);
                        //ArrayList<GenericRecord> l = new ArrayList<>();
                        //while (!decoder.isEnd()) {
                        GenericRecord item = datumReader.read(null, decoder);
                        //    l.add(item);
//                        System.out.println("item sideEffect " + item.get("sideEffect"));
                        //}
                        /**/
                        String topic = + '-' + item.get("vaccineName").toString();
                        int partition = Vaccine.valueOf(item.get("vaccineName").toString()).ordinal();
                        if (!allPartitions.contains(partition)){
                            b = !b;
                            SpecificVaccineSideEffectConsumer.RunConsumer(topic, partition);
                            Thread.sleep(POLL_TIME); // wait starting of new consumer
                            allPartitions.add(partition);
                        }
                        /**/
                        /**/
                        System.out.printf(AnonymousSideEffectConsumer.class.getName() + " java.SideEffect Consumer Record:(%s, %s, %d, %d)\n",
                                record.key(), item,
                                record.partition(), record.offset());
                         /**/
                        //sideEffectOccurence.merge(item.get("siderCode").toString(), 1, (x,y) -> x + y);
                        //System.out.println("side Effect " +  sideEffectOccurence + '\n');
                        //producer.add(topic, Vaccine.valueOf(item.get("vaccineName").toString()).ordinal(), record.key(), item);
                        producer.add(Settings.TOPIC_VACCINE, partition, record.key(), item);
                    }

                    consumer.commitAsync();
                    //consumer.commitSync();
                }
                consumer.close();
                producer.end();
                System.out.printf("closing AnonymousSideEffectConsumer, %d record have been anonymized\n", count);
            } catch (IOException e) {
                System.out.println(AnonymousSideEffectConsumer.class.getName() + " error while loading schema");
                return;
            }
            /**/
            catch (InterruptedException e){
                System.out.println(AnonymousSideEffectConsumer.class.getName() + " interrupted");
                return;
            }
             /**/
        }).start();
    }
}
