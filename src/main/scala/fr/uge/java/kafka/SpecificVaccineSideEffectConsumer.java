package fr.uge.java.kafka;

import fr.uge.java.Vaccine;
import org.apache.avro.Schema;
import org.apache.avro.file.SeekableByteArrayInput;
import org.apache.avro.generic.GenericDatumReader;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.io.BinaryDecoder;
import org.apache.avro.io.DecoderFactory;
import org.apache.kafka.clients.consumer.*;
import org.apache.kafka.common.TopicPartition;
import org.apache.kafka.common.serialization.ByteArrayDeserializer;
import org.apache.kafka.common.serialization.Serde;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.time.Duration;
import java.util.*;
import java.util.logging.Logger;

import static fr.uge.java.kafka.Settings.POLL_TIME;

/**
 * This class represents a consumer on a specific vaccine
 */
public class SpecificVaccineSideEffectConsumer {

    /**
     * create a consumer that listen on specific topic and specific partition
     * the consumer counts the number of {@link fr.uge.java.SideEffect} received
     * @param topic the topic to listen to
     * @param partition the specific partition to get
     */
    public static void RunConsumer(String topic, int partition){
        final Map<String, Object> config = new HashMap<>();
        Logger logger = Logger.getLogger(topic);
        config.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        config.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, ByteArrayDeserializer.class);
        config.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        config.put(ConsumerConfig.GROUP_ID_CONFIG, Settings.VACCINE_GROUP);
        config.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "false");
        //config.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "false");
        //config.put("enable.auto.commit", false);
        //config.put("session.timeout.ms", 30000);
        //config.put("heartbeat.interval.ms", 10000);
        //config.put("request.timeout.ms", 31000);
        //props.put("enable.auto.commit", "true");
        //props.put("auto.commit.interval.ms", "1000");

        KafkaConsumer<String, byte[]> consumer = new KafkaConsumer<>(config);
        consumer.assign(Collections.singletonList(new TopicPartition(topic, partition)));
        System.out.println("topic partition " + topic + " " + partition);
        new Thread(() -> {
            Vaccine[] vaccines = Vaccine.values();
            long count = 0;
            int noRecordsCount = 0;

            File file = Paths.get("resources", "schemas", "schema_anonymous_side_effect.avsc").toFile();

            try {

                 /**/
                /**/
                Schema.Parser parser = new Schema.Parser();
                Schema schema = parser.parse(file);
                while (!Thread.currentThread().isInterrupted()) {
                    //ConsumerRecords<String, byte[]> records = consumer.poll(1000);
                    //ConsumerRecords<String, byte[]> records = consumer.poll(Duration.ofMillis(1000));
                    ConsumerRecords<String, byte[]> records = consumer.poll(Duration.ofSeconds(2));
                    /**/
                    //System.out.println("AAAAA" + records.count());
                    if(records.count() == 0){
                        continue;
                    }
                    /**/
                    for (ConsumerRecord<String, byte[]> record : records) {
                        //Injection<GenericRecord, byte[]> recordInjection = GenericAvroCodecs.toBinary(finalSchema);
                        byte[] data = record.value();
                        GenericDatumReader<GenericRecord> datumReader = new GenericDatumReader<>(schema);
                        SeekableByteArrayInput inputStream = new SeekableByteArrayInput(data);

                        BinaryDecoder decoder = DecoderFactory.get().binaryDecoder(inputStream, null);
                        //ArrayList<GenericRecord> l = new ArrayList<>();
                        //while (!decoder.isEnd()) {
                        GenericRecord item = datumReader.read(null, decoder);
                        //    l.add(item);
//                        System.out.println("item sideEffect " + item.get("sideEffect"));
                        //}
                        /*
                        logger.info("topic:" + topic + " partition:" + partition
                                       + " SpecificVaccineSideEffect Consumer Record: (" + ", ".join(record.key(), item.toString(), record.partition() + "", record.offset() + "") + ") " + ++count);
                        */

                        /** EXERCICE 5 count sideEffect **/
                        //System.out.printf(SpecificVaccineSideEffectConsumer.class.getName() + " type: %s  count: %d\nrecord id:%s vaccineName:%s sideEffect:%s siderCode:%s\n\n", Vaccine.values()[partition], count++, item.get("id"), item.get("vaccineName").toString(), item.get("sideEffect"),  item.get("siderCode"));
                        //sideEffectOccurence.merge(item.get("siderCode").toString(), 1, (x,y) -> x + y);
                        //System.out.println("side Effect " +  sideEffectOccurence + '\n');

                    }
                    consumer.commitAsync();
                    /**/
                    /*
                    System.out.println("TEST " + records.partitions() + " " + records.count());
                    for (TopicPartition partition2 : records.partitions()) {
                        List<ConsumerRecord<String, byte[]>> partitionRecords = records.records(partition2);
                        for (ConsumerRecord<String, byte[]> record : partitionRecords) {
                            System.out.println(record.offset() + ": lala " + record.value() + " " + partition);
                        }
                        long lastOffset = partitionRecords.get(partitionRecords.size() - 1).offset();
                        consumer.commitSync(Collections.singletonMap(partition2, new OffsetAndMetadata(lastOffset + 1)));
                    }
                    */
                }
                consumer.close();
                System.out.printf("closing consumer,  topic:%s partition:%d vaccineName:%s numberOfSideEffects:%d\n", vaccines[partition], partition, Vaccine.values()[partition], count);
            } catch (IOException e) {
                System.out.println(SpecificVaccineSideEffectConsumer.class.getName() + " error while loading schema");
                return;
            }
            /*
            catch (InterruptedException e){
                System.out.println(SpecificVaccineSideEffectConsumer.class.getName() + " interrupted");
                return;
            }
             */
        }).start();
    }
}
