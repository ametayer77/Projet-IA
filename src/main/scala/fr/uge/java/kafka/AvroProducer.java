package fr.uge.java.kafka;

import com.twitter.bijection.Injection;
import com.twitter.bijection.avro.GenericAvroCodecs;
import org.apache.avro.Schema;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericRecord;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.internals.DefaultPartitioner;
import org.apache.kafka.common.serialization.ByteArraySerializer;
import org.apache.kafka.common.serialization.StringSerializer;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Represents a {@link KafkaProducer} that generate avro message from a {@link SideEffectRecord}
 */
public class AvroProducer {;

    /**
     * Run a Avro {@link KafkaProducer} base on  {@link Schema} "schema_record.avsc"<br>
     * This producer will send a record create from @{@link SideEffectRecord}<br>
     * To receive the message you must use @{@link AvroConsumer} or a consumer that listen on TOPIC_AVRO_SIDE_EFFECT topic from {@link Settings}
     * @param sideEffectRecords all records to send in the producer
     * @throws IOException if an IO error occurs
     */
    public static void runProducer(List<SideEffectRecord> sideEffectRecords) throws IOException, InterruptedException {
        final Map<String, Object> config = new HashMap<>();
        config.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        config.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, ByteArraySerializer.class);
        config.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        //config.put(ProducerConfig.PARTITIONER_CLASS_CONFIG, DefaultPartitioner.class);
        config.put(ProducerConfig.ACKS_CONFIG, "all");

        File file = Paths.get("resources", "schemas", "schema_record.avsc").toFile();
        Schema.Parser parser = new Schema.Parser();
        Schema schema = parser.parse(file);
        Schema basicSchema = parser.parse(Paths.get("resources", "schemas", "schema_rdf_record.avsc").toFile());
        Injection<GenericRecord, byte[]> recordInjection = GenericAvroCodecs.toBinary(schema);
        Injection<GenericRecord, byte[]> basicRecordInjection = GenericAvroCodecs.toBinary(basicSchema);

        KafkaProducer<String, byte[]> producer = new KafkaProducer<>(config);

        for (SideEffectRecord sideEffectRecord : sideEffectRecords) {
                // generate a avro record from sideEffect class
                GenericRecord genericRecord = new GenericData.Record(schema);
                genericRecord.put("id", sideEffectRecord.getId());
                genericRecord.put("firstName", sideEffectRecord.getFirstName());
                genericRecord.put("lastName", sideEffectRecord.getLastName());
                genericRecord.put("vaccineName", sideEffectRecord.getVaccineName());
                genericRecord.put("sideEffect", sideEffectRecord.getSideEffect());
                genericRecord.put("siderCode", sideEffectRecord.getSiderCode());
                byte[] data = recordInjection.apply(genericRecord);
                producer.send(new ProducerRecord<>(Settings.TOPIC_AVRO_SIDE_EFFECT, sideEffectRecord.getId(), data));

                GenericRecord basicRDFRecord = new GenericData.Record(basicSchema);
                basicRDFRecord.put("id", sideEffectRecord.getId());
                basicRDFRecord.put("firstName", sideEffectRecord.getFirstName());
                basicRDFRecord.put("lastName", sideEffectRecord.getLastName());
                basicRDFRecord.put("age", sideEffectRecord.getAge());
                basicRDFRecord.put("vaccineName", sideEffectRecord.getVaccineName());
                basicRDFRecord.put("sideEffect", sideEffectRecord.getSideEffect());
                basicRDFRecord.put("siderCode", sideEffectRecord.getSiderCode());
                byte[] basicRDFData = basicRecordInjection.apply(basicRDFRecord);
                producer.send(new ProducerRecord<>(Settings.TOPIC_AVRO_BASIC_RDF_DATA, sideEffectRecord.getId(), basicRDFData));
                Thread.sleep(300);
        }
        producer.flush();
        producer.close();
    }
}
