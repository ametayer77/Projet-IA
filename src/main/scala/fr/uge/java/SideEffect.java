package fr.uge.java;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.Random;

@JsonPropertyOrder({ "name", "siderCode" })
public enum SideEffect {

    INJECTION_SITE_PAIN("Injection site pain", "C0151828"),
    FATIGUE("fatigue", "C0015672"),
    HEADACHE("headache", "C0018681"),
    MUSCLE_PAIN("Muscle pain", "C0231528"),
    CHILLS("chills", "C0085593"),
    JOINT_PAIN("Joint pain", "C0003862"),
    FEVER("fever", "C0015967"),
    INJECTION_SITE_SWELLING("Injection site swelling", "C0151605"),
    INJECTION_SITE_REDNESS("Injection site redness", "C0852625"),
    NAUSEA("Nausea", "C0027497"),
    MALAISE("Malaise", "C0231218"),
    LYMPHADENOPATHY("Lymphadenopathy", "C0497156"),
    INJECTION_SITE_TENDERNESS("Injection site tenderness", "C0863083");

    private String name;
    private String siderCode;

    private SideEffect(String name, String siderCode){
        this.name = name;
        this.siderCode = siderCode;
    }

    public static SideEffect randomSideEffect(){
        //java.SideEffect.
        Random r = new Random();
        SideEffect[] elems = SideEffect.values();
        return elems[r.nextInt(elems.length)];
    }

    public String getName() {
        return name;
    }

    public String getSiderCode() {
        return siderCode;
    }

    @Override
    public String toString() {
        return "java.SideEffect{" +
                "name='" + name + '\'' +
                ", siderCode='" + siderCode + '\'' +
                '}';
    }
}
