package fr.uge.java;

public enum Vaccine {
   MODERNA, PFIZER, CANSINOBIO, SPUTNIK_V, ASTRAZENECA;
}
