package fr.uge.java;

import com.twitter.bijection.Injection;
import com.twitter.bijection.avro.GenericAvroCodecs;
import fr.uge.java.kafka.Settings;
import org.apache.avro.Schema;
import org.apache.avro.file.SeekableByteArrayInput;
import org.apache.avro.generic.GenericData;
import org.apache.avro.generic.GenericDatumReader;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.io.BinaryDecoder;
import org.apache.avro.io.DecoderFactory;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.internals.DefaultPartitioner;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.JoinWindows;
import org.apache.kafka.streams.kstream.KStream;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.time.Duration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;

public class JoinStream {

    public static void run() throws IOException {
        /**
         * Map to count sideEffect per age Interval
         * Key: VACCINE NAME
         *      Key: AgeInterval
         *      Value: sideEffect count
         * a new Avro is also created and sent to "Merged" topic
         **/
        HashMap<String, HashMap<String, Long>> count = new HashMap<>();
        File basicRDFFile = Paths.get("resources", "schemas", "schema_rdf_record.avsc").toFile();
        File anonymizedFile = Paths.get("resources", "schemas", "schema_anonymous_side_effect.avsc").toFile();
        File anonymizedAgeFile = Paths.get("resources", "schemas", "schema_anonymous_age_side_effect.avsc").toFile();

        Schema basicSchema = new Schema.Parser().parse(basicRDFFile);
        Schema anonymizedSchema = new Schema.Parser().parse(anonymizedFile);
        Schema anonymizedAgeSchema = new Schema.Parser().parse(anonymizedAgeFile);

        StreamsBuilder builder = new StreamsBuilder();

        Injection<GenericRecord, byte[]> recordInjection = GenericAvroCodecs.toBinary(anonymizedAgeSchema);

        // create sor processors
        //KafkaStreams kafkaStreams = new
        //        KafkaStreams(builder.build(),streamingConfig);
        //kafkaStreams.start();
        Properties streamsConfiguration = new Properties();
        streamsConfiguration.put(StreamsConfig.APPLICATION_ID_CONFIG, "sideEffectCountByAge");
        streamsConfiguration.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");

        KStream<String, byte[]> basicRDF = builder.stream(Settings.TOPIC_AVRO_BASIC_RDF_DATA);
        KStream<String, byte[]> anonymizedData = builder.stream(Settings.TOPIC_VACCINE);
        //KStream<String, byte[]> anonymizeData = builder.stream(List.of(Settings.TOPIC_VACCINE, Settings.TOPIC_AVRO_BASIC_RDF_DATA));
                /*
                aa.mapValues((v) -> {
                    SeekableByteArrayInput inputStream = new SeekableByteArrayInput(v);
                    BinaryDecoder decoder = DecoderFactory.get().binaryDecoder(inputStream, null);
                    //ArrayList<GenericRecord> l = new ArrayList<>();
                    //while (!decoder.isEnd()) {
                    try {
                        GenericDatumReader<GenericRecord> datumReader2 = new GenericDatumReader<>(basicSchema);
                        GenericRecord item = datumReader2.read(null, decoder);
                        System.out.println("le stream " + topic + " " + partition + " " + item.get("id") + " " + item.get("age") + " " + item.get("vaccineName"));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    //System.out.println("key:" + k);
                    return v;
                });
                */
        JoinWindows twentyMinuteWindow = JoinWindows.of(Duration.ofMinutes(20));
        KStream<String, byte[]> joined  = basicRDF.join(anonymizedData, (rdfValue, anonymizedValue) -> {
            GenericDatumReader<GenericRecord> basicDatumReader = new GenericDatumReader<>(basicSchema);
            GenericDatumReader<GenericRecord> anonymizedDatumReader = new GenericDatumReader<>(anonymizedSchema);
            //GenericDatumReader<GenericRecord> MergedDatumReader = new GenericDatumReader<>(anonymizedSchema); // new value joined value ( in case we need it later )

            SeekableByteArrayInput basicInputStream = new SeekableByteArrayInput(rdfValue);
            SeekableByteArrayInput anonymizedInputStream = new SeekableByteArrayInput(anonymizedValue);

            BinaryDecoder basicDecoder = DecoderFactory.get().binaryDecoder(basicInputStream, null);
            BinaryDecoder anonymizedDecoder = DecoderFactory.get().binaryDecoder(anonymizedInputStream, null);
            try {
                GenericRecord basicItem = basicDatumReader.read(null, basicDecoder);
                GenericRecord anonymizedItem = anonymizedDatumReader.read(null, anonymizedDecoder);

                String id = anonymizedItem.get("id").toString();
                String vaccineName = anonymizedItem.get("vaccineName").toString();
                String age = basicItem.get("age").toString();
                String sideEffect = anonymizedItem.get("sideEffect").toString();
                String siderCode = anonymizedItem.get("siderCode").toString();

                GenericRecord genericRecord = new GenericData.Record(anonymizedAgeSchema);
                genericRecord.put("id", id);
                genericRecord.put("vaccineName", vaccineName);
                genericRecord.put("sideEffect", sideEffect);
                genericRecord.put("age", age);
                genericRecord.put("siderCode", siderCode);
                byte[] data = recordInjection.apply(genericRecord);
                //count.merge(vaccineName, new HashSet<>())
                var countPerAgeInterval = count.computeIfAbsent(vaccineName, (x) -> new HashMap<>());
                int ageInterval = (int)Math.ceil(Integer.parseInt(age) / 10) * 10;
                countPerAgeInterval.merge("[" +ageInterval + "-" + (ageInterval + 10) + "]", 1L, (v1, v2) -> v1 + v2);
                //System.out.println("rdfValue " + rdfValue + " " + "anonymizeValue" + " " + anonymizedValue);
                System.out.println("vaccineName: " + vaccineName + " " + "age: " + age + " " + "sideEffect: " + sideEffect);
                System.out.println(vaccineName + " " + countPerAgeInterval + "\n");
                //return Joined.with(Serdes.String(), Serdes.ByteArray(), Serdes.ByteArray());
                return data;

            } catch (IOException e) {
                System.err.println("Error in join stream " + e.getMessage());
               return anonymizedValue;
            }
        }, twentyMinuteWindow);
        joined.to("Merged");
        new KafkaStreams(builder.build(), streamsConfiguration).start();
        /**/

    }
}
