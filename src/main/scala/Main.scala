import fr.uge.java.{JoinStream, Vaccine}
import fr.uge.java.kafka._
import org.apache.jena.rdf.model.{ModelFactory, Resource}
import org.apache.kafka.clients.admin.{AdminClient, NewTopic}

import java.io.{File, PrintWriter}
import scala.collection.mutable.ListBuffer


object Main extends App {
  val model = ModelFactory.createDefaultModel()
  var resources = model.read("file:resources/lubm1.ttl", "TURTLE")

  if (args.length != 7){
    throw new IllegalStateException("we must have 7 parameters.")
  }

  var params = new ListBuffer[Int]()

  try{
     args.foreach(a => params += a.toInt)
  }catch {
      case e : NumberFormatException => throw new IllegalStateException("Parameters must all be ints")
  }
  if (params(0) + params(1) != 100 ){
    throw new IllegalStateException("The sum of the first two parameters must be 100")
  }
  if (params(2) + params(3) + params(4) + params(5) +params(6) != 100 ){
    throw new IllegalStateException("The sum of the first parameters 3 to 7 must be 100")
  }



  /**
   * Retrieve all persons (students, professors...)<br>
   * Generate fake infos (name, age, isVaccinated, vaccine name, side effects if any...) on some persons<br>
   * then anonymize the data in a first kafka producer<br>
   * the consumer thread  of that producer contains another kafka producer which will send the anonymized data to a topic "VaccineSideEffect"<br>
   * This topic is separated in multiple partitions, corresponding in all vaccine available<br>
   * Each partition have a consumer which counts all sideEffect
   *
   */
  def createFakeInfo() = {
    val sttIt = model.listStatements()
    var subjects = new ListBuffer[Resource]()
    val records = new java.util.ArrayList[SideEffectRecord]()
    println("retrieve professors, students and lectures...")
    while(sttIt.hasNext()) {
      val a = sttIt.next()
      val subject = a.getSubject
      if (subject.getLocalName.contains("Professor") || subject.getLocalName.contains("Student") || subject.getLocalName.contains("Lecturer")){
        subjects += subject
      }
    }
    println("done.")
    println("create fakeInfo...")
    val paramList = params.toList
    subjects = subjects.distinct
    subjects.foreach(x => {
      val record = FakeInfo.createFakeInfo(model, x, paramList)
      if (record != null){
        records.add(record)
      }
    })
    println("done.")
    val writer = new PrintWriter(new File("result/fakeResult.ttl"))
    model.write(writer)


    //println(records)
    //JSONConsumer.RunConsumer();
    //JSONProducer.runProducer(records)

    Settings.initialize()
    println("run consumer and producer...")
    Vaccine.values().foreach(v =>
      SpecificVaccineSideEffectConsumer.RunConsumer(Settings.TOPIC_VACCINE, v.ordinal())
    )
    AvroConsumer.RunConsumer()
    Thread.sleep(500); // time to start all consumers
    //AnonymousSideEffectConsumer.RunConsumer()
    // launch consumers on multiple partitions
    JoinStream.run()
    AvroProducer.runProducer(records)
    //for(l : )
    println("end producer")
  }
    // records and printed in console only
/*
  def tt(): Unit ={
    val records = new java.util.ArrayList[SideEffectRecord]()
    records.add(new SideEffectRecord("1", "julie", "Jules", "Sputnik V", "Nausea", "C00000000"))
    AvroConsumer.RunConsumer();
    AvroProducer.runProducer(records)
    //AvroConsumer.test(records)
  }
*/

  //tt()
  createFakeInfo()
}
