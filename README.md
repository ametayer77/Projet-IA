# Project Symbolic AI Reasoning and Data Engineer 

#
#
## _Side effects of vaccine for covid-19_
#
The project consist to create some RDF data sets on people who have been vaccined for covid-19.And generate information about side effects witnessed for these persons.

## License
**Free Software**

## Authors
- [Jean-Manuel ERIALC][Jean-Manuel]
- [Armand LIEGEY][Armand]
- [Ambre METAYER][Ambre]

## Prerequisite
#
have an IDE (Eclipse, Intellij)
have Scala 12 not 13
install [zookeepzer][zoo] at least version 3.6.2
install [kafka][kafka] for scala 12

## Installation
In Zookeper directory tape the line

```sh
./bin/zkServer.sh start
```
Zookeper is launched
In Kafka directory tape the line
```sh
bin/kafka-server-start.sh -daemon config/server.properties
```
Kafka is launched
Now you can lauch the programm with it seven parameters describe below

## Parameters

To use the program we need seven parameters which are in order every one of those parameters are percentages :
- Female
- Male
- MODERNA
- PFIZER 
- CANSINOBIO 
- SPUTNIK_V
- ASTRAZENECA


If the programm don't have this seven parameters it will stop.
All of this parameters must be an Integer.
The sum of the first two parameters must be 100
The sum of the third parameters to seven must be 100
#
For ours test we use the following parameters : 52 48 35 25 10 15 15

## Avancement
#
In this project, we get a list of person, we attributes vaccin and side effect. The vaccin is calculed by the parameters given.
We have create an ontology that addresses a LUBM extensions.
We can count the number of vaccined and not-vaccined persons
We can retrieve last and first names of professors or student, or an organization.
We can anonymise data to have only the vaccin and it side effect
Also counting side effects per vaccine and age intervals
#


[//]: # (These are reference links used in the body of this note and get stripped out when the markdown processor does its job. There is no need to format nicely because it shouldn't be seen. Thanks SO - http://stackoverflow.com/questions/4823468/store-comments-in-markdown-syntax)
    
   [Jean-Manuel]:<https://gitlab.com/jeanmanuel.erialc>
   [Armand]:<https://gitlab.com/Afkeu>
   [Ambre]:<https://gitlab.com/ametayer77>
   [zoo]:<https://zookeeper.apache.org/releases.html>
   [kafka]:<https://kafka.apache.org/downloads>


## RUN

By performing the main,
Fake data will be generated thanks to javaFaker and according to the given parameters.
Then data will be sent via a basic producer (topic: "avroBasicRDFData")
The side effects data will be anonimized and sent to the "anonymousSideEffect" topic which belongs to a consumer group partitioned according to the number of vaccines.
A thread is created for each partition and a side effects count is performed (commits for exercise 5).
finally, a join is done with KafkaStreams in order to obtain the side effects by age interval. (final commit)
