name := "Projet_IA"

version := "0.1"

scalaVersion := "2.13.4"

libraryDependencies += "org.apache.jena" % "jena-core" % "3.17.0"
libraryDependencies += "org.slf4j" % "slf4j-simple" % "1.7.30"
libraryDependencies += "com.github.javafaker" % "javafaker" % "1.0.2"
libraryDependencies += "org.apache.kafka" % "kafka-clients" % "2.7.0"
libraryDependencies += "org.apache.kafka" % "kafka-streams" % "2.7.0"
libraryDependencies += "com.fasterxml.jackson.core" % "jackson-databind" % "2.10.0"
libraryDependencies += "com.twitter" % "bijection-avro_2.13" % "0.9.7"